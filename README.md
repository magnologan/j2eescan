# J2EEScan - J2EE Security Scanner Burp Suite Plugin

## What is J2EEScan
J2EEScan is a plugin for [Burp Suite Proxy](http://portswigger.net/). 
The goal of this plugin is to improve the test coverage during 
web application penetration tests on J2EE applications. 


## How does it works?

The plugin is fully integrated into the Burp Suite Scanner; it adds some new test 
cases and new strategies to discover different kind of J2EE vulnerabilities.


 ![IMAGE](https://bitbucket.org/ilmila/j2eescan/raw/master/resources/j2eescan-results.png)



## Test cases:

 * Expression Language Injection (CVE-2011-2730)
 * JBoss SEAM Remote Command Execution (CVE-2010-1871)
 * Java Server Faces Local File Include (CVE-2013-3827 CVE-2011-4367)
 * Local File include - /WEB-INF/web.xml Retrieved
 * Apache Struts 2 S2-016
 * Apache Struts 2 S2-017
 * Apache Struts 2 S2-020
 * Apache Struts 2 S2-021
 * Apache Struts DevMode Enabled
 * Grails Path Traversal (CVE-2014-0053)
 * Incorrect Error Handling - JSF
 * Incorrect Error Handling - Apache Struts
 * Incorrect Error Handling - Java
 * XML Security - XInclude Support
 * XML Security - XML External Entity
 * Infrastructure Issue - Tomcat Manager Console Weak Password
 * Infrastructure Issue - WEB-INF Application Configuration Files Retrieved
 * Infrastructure Issue - Status Servlet
 * Infrastructure Issue - Extended Path Traversal Scan
 * Infrastructure Issue - JBoss Web Service Enumeration
 * Infrastructure Issue - JBoss Admin Console Weak Password
 * Infrastructure Issue - JBoss JMX/Web Console Not Password Protected
 * Infrastructure Issue - JBoss JMX Invoker Remote Command Execution
 

## How to install ?

 * From "Cookie jar" section in "Options" -> "Sessions" enable the Scanner field
 * Load the J2EEscan jar in the Burp Extender tab


## Release Notes


### Version 1.1.2 (18 Oct, 2014):
Initial Public Release
 

## License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.


